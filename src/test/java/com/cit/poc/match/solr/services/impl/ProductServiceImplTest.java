package com.cit.poc.match.solr.services.impl;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cit.poc.match.MatchSolrPocApplication;
import com.cit.poc.match.solr.Samples;
import com.cit.poc.match.solr.entity.Product;
import com.cit.poc.match.solr.services.ProductService;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MatchSolrPocApplication.class)
@Slf4j
public class ProductServiceImplTest {
	private static final String HASH = StringUtils.repeat("#", 10);
	@Inject
	private ProductService productService = null;

	@Before
	public void registerProducts() {
		Samples.products.keySet().forEach(p -> {
			productService.registerProduct(p);
		}
		);
		
		Samples.products.keySet().forEach(p -> {
			Product pSearched = productService.findOne(p.getId());
			assertEquals(p, pSearched);
		}
		);
	}
	
	private void logProduct(Product p) {
		log.info("\t({})Title '{}'", p.getId(), p.getTitle());
	}
	
	@Test
	public void testFuzyTitleSearch() {
		log.info(HASH.concat("testFuzyTitleSearch").concat(HASH));
		Samples.products.values().iterator().next().forEach(
				p -> {
					log.info("Search by '{}'", p.getTitle());
					productService.fuzyTitleSearch(p.getTitle())
						.forEachRemaining(s -> logProduct(s));
				}
		);
		log.info(HASH.concat("testFuzyTitleSearch").concat(HASH));
	}

	
	@Test
	public void testFuzzyProduct() {
		log.info(HASH.concat("testFuzzyProduct").concat(HASH));
		Samples.products.values().iterator().next().forEach(
				p -> {
					log.info("Search by '{}'", p.getTitle());
					productService.fuzySearch(p)
						.forEachRemaining(s -> logProduct(s));
				}
		);
		log.info(HASH.concat("testFuzzyProduct").concat(HASH));
	}
	
	
}
