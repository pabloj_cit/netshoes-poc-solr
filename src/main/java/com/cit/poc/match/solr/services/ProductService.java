package com.cit.poc.match.solr.services;

import java.util.Iterator;

import com.cit.poc.match.solr.entity.Product;

public interface ProductService {

	public Product registerProduct(Product product);
	public Product findOne(String id);
	public Iterator<Product> fuzyTitleSearch(String title);
	public Iterator<Product> fuzySearch(Product product);
	//public Iterator<Product> searchSimilar(Product product);
}
