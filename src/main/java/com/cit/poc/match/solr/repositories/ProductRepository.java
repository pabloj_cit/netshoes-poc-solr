package com.cit.poc.match.solr.repositories;

import org.springframework.data.repository.CrudRepository;

import com.cit.poc.match.solr.entity.Product;

public interface ProductRepository extends CrudRepository<Product, String> {

	
}
