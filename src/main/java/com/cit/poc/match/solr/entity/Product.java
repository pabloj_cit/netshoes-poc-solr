package com.cit.poc.match.solr.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SolrDocument(solrCoreName="poc")
public class Product {
	@Id
	private String id = null;
	@Indexed
	private String skuStore = null;
	@Indexed
	private String skuSeller = null;
	@Indexed
	private String ean = null;
	@Indexed
	private String title = null;
	@Indexed
	private String type = null;
	@Singular
	@Indexed(name="categories", searchable=true)
	private List<String> categories = null;
	
	//solr not suports key/value, concatenate!
	@Singular
	@Indexed(name="attributes", searchable=true)
	private List<String> attributes = null;
	
	@Singular
	private transient List<MultiValues> multiValues = null;
	
	
	/**
	 * Populate attributes to be indexed by solr
	 * Reset attributes if already exists.
	 */
	public void populateAttributes() {
		this.attributes = new ArrayList<String>();
		multiValues.parallelStream().forEach(
				m -> {
					m.getItems().forEach(
							i -> {
								i.getValues().stream().filter(v -> v != null).forEach(
										v -> 
											this.attributes.add(
												m.getId().concat(" ")
												.concat(i.getId()).concat(" ")
												.concat(v))
								);
								i.getAttributes().entrySet().forEach(
										a -> 
											this.attributes.add(
													m.getId().concat(" ")
													.concat(i.getId()).concat(" ")
													.concat(a.getKey().concat(" ").concat(a.getValue())))
								);
							}
					);
				}
		);
	}
}
