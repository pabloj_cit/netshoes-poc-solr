package com.cit.poc.match.solr.entity;

import java.util.List;
import java.util.SortedMap;

import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SingleValue {
	@Id
	private String id = null;
	@Singular
	@Indexed(name="values")
	private List<String> values = null;
	@Singular
	@Indexed(name="attributes")
	private SortedMap<String, String> attributes = null;
	
}
