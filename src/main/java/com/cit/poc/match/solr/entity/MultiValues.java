package com.cit.poc.match.solr.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MultiValues {
	@Id
	private String id = null;
	@Singular
	@Indexed(name="items", searchable=true)
	private List<SingleValue> items = null;

}
