package com.cit.poc.match.solr.services.impl;

import java.util.Iterator;

import javax.inject.Inject;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.common.params.MoreLikeThisParams;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.QueryParsers;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.stereotype.Service;

import com.cit.poc.match.solr.entity.Product;
import com.cit.poc.match.solr.repositories.ProductRepository;
import com.cit.poc.match.solr.services.ProductService;

@Service
public class ProductSolrServiceImpl implements ProductService {
	@Inject
	private ProductRepository repo = null;
	@Inject
	private SolrTemplate template = null;
	@Inject
	private SolrServer server = null;

	@Override
	public Product registerProduct(Product product) {
		return repo.save(product);
	}

	@Override
	public Product findOne(String id) {
		return repo.findOne(id);
	}

	@Override
	public Iterator<Product> fuzyTitleSearch(String title) {
		Criteria c = new Criteria("title").fuzzy(title);
		SimpleQuery q = new SimpleQuery(c);
		q.addSort(new Sort("id")); //required by query cursor
		return template.queryForCursor(q, Product.class);
	}

	@Override
	public Iterator<Product> fuzySearch(Product product) {
		Criteria c = new Criteria("title").fuzzy(product.getTitle())
				.or(new Criteria("attributes").fuzzy(product.getTitle()))
				;
		SimpleQuery q = new SimpleQuery(c);
		q.addSort(new Sort("id")); //required by query cursor
		return template.queryForCursor(q, Product.class);
	}

	public Iterator<Product> searchSimilar(Product product) throws Exception {
		SolrQuery q = new SolrQuery();
		;
		q.setRequestHandler("/".concat(MoreLikeThisParams.MLT));
		q.set(MoreLikeThisParams.MATCH_INCLUDE, true);
	    q.set(MoreLikeThisParams.MIN_DOC_FREQ, 1);
	    q.set(MoreLikeThisParams.MIN_TERM_FREQ, 1);
	    q.set(MoreLikeThisParams.MIN_WORD_LEN, 7);
	    q.set(MoreLikeThisParams.BOOST, false);
	    q.set(MoreLikeThisParams.MAX_QUERY_TERMS, 1000);
	    q.set(MoreLikeThisParams.SIMILARITY_FIELDS, "title,attributes");
	    q.setQuery("id:" + product.getId());
	    q.set("fl", "id,score");
	    q.addFilterQuery("title:" + product.getTitle());
	    q.setRows(10);
	    
	    return template.convertQueryResponseToBeans(
	    		server.query(q), Product.class).iterator();
	}
	
	
}