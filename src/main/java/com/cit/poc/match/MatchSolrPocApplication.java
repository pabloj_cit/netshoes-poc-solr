package com.cit.poc.match;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatchSolrPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatchSolrPocApplication.class, args);
	}
}
